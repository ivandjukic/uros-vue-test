import axios from 'axios'
import config from '../config.json'

export const apiClient = axios.create({
    baseURL: config.backendApiUrl,
    withCredentials: false,
    headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        // TODO uncomment this line if website use some JWT authentication
        // Authorization: {
        //     toString() {
        //         return `Bearer ${localStorage.getItem('jwt_token')}`
        //     }
        // }
    },
    timeout: 10000
});
