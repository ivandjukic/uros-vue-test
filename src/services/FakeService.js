import ApiService from "../services/ApiService";

export default {
    fetchTodos() {
        return ApiService.get('todos');
    }
}
