# uros-test

## 1. Install dependencies
```
npm install
```

### 2. Create config page
```
copy /src/config_example.json to config.json
```

### 3. Set fake remote server url
```
EDIT config.json 
  "backendApiUrl": "https://jsonplaceholder.typicode.com"
```

### 4. Start npm hot reloading server
```
npm run serve
```